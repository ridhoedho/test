import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })

export class Env {
    private host: string = "https://api.smartbiz.id/api/"

    public gethost() {
        return this.host
    } 
}