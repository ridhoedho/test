import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { IndexComponent } from './employee/index/index.component';
import { BaseComponent } from './base/base.component';
import { createComponent } from '@angular/compiler/src/core';
import { FormComponent } from './employee/form/form.component';
// import { GuardService as Guard} from './services/guard.service';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '', 
    component: BaseComponent,
    // canActivate: [Guard],
    children: [
      {
        path: '',
        component:  IndexComponent
      },
      {
        path: 'create',
        component:  FormComponent
      },
      {
        path: 'employee/:id/detail',
        component: FormComponent
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
