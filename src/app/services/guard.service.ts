import { Injectable } from '@angular/core';
import { AuthService } from './auth.service'
import { 
  Router, 
  CanActivate 
} from '@angular/router'

@Injectable({
  providedIn: 'root'
})

export class GuardService   {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  canActivate(): boolean {
    if(this.authService.isAuthenticated() == true)
    {
      return true
    }
    this.router.navigate(['login'])
    return false
  }
}
