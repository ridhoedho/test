import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Env } from '../env';
import { Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { NgxSpinnerService } from 'ngx-spinner'

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(
    private httpClient: HttpClient,
    private env: Env,
    private route: Router,
    private spinner: NgxSpinnerService
  ) { }

  async data(data) {
    this.spinner.show()
    return new Promise((resolve, reject) => {
      this.httpClient.post('https://api.smartbiz.id/api/demo/employee/page-search', data)
      .toPromise()
      .then((response: any) => {
        resolve(response)
        this.spinner.hide()
      })
      .catch((error) => {
        reject(error)
      })
    })
  }

  async store(data) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(this.env.gethost() + 'demo/employee', data)
      .toPromise()
      .then((response: any) => {
        resolve(response)
      })
      .catch((error) => {
        reject(error)
      })
    })
  }

  async delete(id: number) {
    return new Promise((resolve, reject) => {
      this.httpClient.delete(this.env.gethost() + "demo/employee/" + id)
      .toPromise()
      .then((response: any) => {
        resolve(response)
      })
      .catch((error) => {
        reject(error)
      })
    })
  }

  async edit(id: number) {
    return new Promise((resolve, reject) => {
      this.httpClient.get(this.env.gethost() + "demo/employee/" + id)
      .toPromise()
      .then((response: any) => {
        resolve(response)
      })
      .catch((error) => {
        reject(error)
      })
    })
  }

  async update(id, data) {
    return new Promise((resolve, reject) => {
      this.httpClient.put(this.env.gethost() + "demo/employee/" + id, data)
      .toPromise()
      .then((response: any) => {
        this.route.navigate([''])
      })
      .catch((error) => {
        reject(error)
      })
    })
  }
}
