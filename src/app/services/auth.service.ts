import { Injectable } from '@angular/core';
import { Env } from '../env';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service'
import { NgxSpinnerService } from 'ngx-spinner';


@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(
    private env: Env,
    private httpClient: HttpClient,
    private cookiesService: CookieService,
    private spinner: NgxSpinnerService
  ) { }

  async login(data) {
    this.spinner.show()
    return await new Promise((resolve, reject) => {
      this.httpClient.post(this.env.gethost() + "login" , data)
      .toPromise()
      .then((response: any) => {
         resolve(response)
         this.spinner.hide()
      })
      .catch((error) => {
        reject(error)
      })
    })
  }
  isAuthenticated(): boolean {
    const token = this.cookiesService.get('token')
    if(token != '') {
      return true
    }
    return false
  }

  getToken() {
    return this.cookiesService.get('token')
  }

  logout() {
    return this.cookiesService.deleteAll()
  }
}
