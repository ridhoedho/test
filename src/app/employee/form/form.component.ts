import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { EmployeeService } from 'src/app/services/employee.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  formGroup: FormGroup
  employeeId: any = null
  employeeDetail: any
  constructor(
    private employeeService: EmployeeService,
    private formBuilder: FormBuilder,
    private router: Router,
    private activateRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activateRoute.params.subscribe(params => {
      this.employeeId = params['id']
    })
    this.formGroup = this.formBuilder.group({
      'nip': new FormControl('', Validators.required),
      'full_name': new FormControl('', Validators.required),
      'nick_name': new FormControl('', Validators.required),
      'birth_date': new FormControl('', Validators.required),
      'address': new FormControl('', Validators.required),
      'phone': new FormControl('', Validators.required),
      'mobile': new FormControl('', Validators.required),
      'email': new FormControl('', Validators.required)
    });
    if(this.employeeId != null) {
      this.edit(this.employeeId)
    }
  }

  store(data) {
    this.employeeService.store(data)
    .then((response: any) => {
      if(response.status == 200) {
        this.router.navigate([''])
      }
    })
    .catch((error) => {
      console.log(error)
    })
  }

  edit(id:number) {
    this.employeeService.edit(id)    
    .then((response: any ) => {
      this.employeeDetail = response
      this.formGroup.get('nip').setValue(this.employeeDetail.nip)
      this.formGroup.get('full_name').setValue(this.employeeDetail.full_name)
      this.formGroup.get('nick_name').setValue(this.employeeDetail.nick_name)
      var d = this.employeeDetail.birth_date;
      d = d.split(' ')[0];
      this.formGroup.get('birth_date').setValue(d)
      this.formGroup.get('address').setValue(this.employeeDetail.address)
      this.formGroup.get('phone').setValue(this.employeeDetail.phone)
      this.formGroup.get('mobile').setValue(this.employeeDetail.mobile)
      this.formGroup.get('email').setValue(this.employeeDetail.email)
      console.log(this.employeeDetail)
    })
    .catch((error) => {
      console.log(error)
    })
  }

  update(id: number, data: any) {
    this.employeeService.update(id, data)
    .then((response: any) => {
      if(response.status == 200) {
        alert("success")
      }
    })
    .catch((error) => {
      console.log(error)
    })
  }
}
