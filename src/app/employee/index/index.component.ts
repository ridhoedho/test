import { Component, OnInit, ViewChild } from '@angular/core';
import { EmployeeService } from 'src/app/services/employee.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  data: any
  displayedColumns: string[] =  ['number', 'nip', 'full_name', 'nick_name', 'birth_date', 'address','phone','mobile','email','action'] 
  filterStatus: boolean = false
  params =   {
    "query": {
        "value": null
    },
    "pagination": {
            "page": 1,
            "perpage": 99
    },
    "sort": {
            "sort": "ASC",
            "field": "id"
    }
  }
  @ViewChild(MatPaginator, {static:true}) paginator: MatPaginator 
  @ViewChild(MatSort, {static:true}) sort: MatSort

  constructor(
    private employeeService: EmployeeService,
    private router: Router,
  ) { }
  
  ngOnInit(): void {
    this.getdata(this.params)
  }

  getdata(data) {
    this.employeeService.data(data)
    .then((response: any) => {
      this.data = new MatTableDataSource(response.rows)
      this.data.paginator = this.paginator
      this.data.sort = this.sort
    })
    .catch((error) => {
      console.log(error)
    })
  }
  
  filter(event: Event) {
    let filterValue = (event.target as HTMLInputElement).value
    this.data.filter = filterValue.trim().toLowerCase()
    console.log(this.data)
    if(this.data.filteredData.length == '') {
      this.filterStatus = true
    } else {
      this.filterStatus = false
    }
  }
  
  create() {
    this.router.navigate(['/create'])
  }

  delete(id: number) {
    this.employeeService.delete(id)
    .then((response: any) => {
      if(response.status == 200 ) {
        this.getdata(this.params)
      }
    })
    .catch((error) => {
      console.log(error)
    })
  }
}
