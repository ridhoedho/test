import { Component, OnInit } from '@angular/core';
import { 
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms'
import { CookieService } from 'ngx-cookie-service'
import { Router } from '@angular/router'
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formGroup: FormGroup

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private cookiesService: CookieService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      'identity': new FormControl('', Validators.required),
      'password': new FormControl('', Validators.required)
    });
  }

  login(data) {
    this.authService.login(data)
    .then((response: any) => {
      this.cookiesService.set('token', response.data.token.access_token)
      this.router.navigate([''])
    })
    .catch((error) => {
      console.log(error)
    })
  }
  
}
